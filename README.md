# Intro

In order to facilitate for example map application creation at PSK
(see https://geopresovregion.sk/home/tematicke-mapove-aplikacie/)
using local PostgreSQL/PostGIS database this script harvests selected
datasets (CSV files) from remote Git repository into local database.


## About NCZI Covid 19 data

We are using CSV files from NCZI which are released as Open Data:

https://github.com/Institut-Zdravotnych-Analyz/covid19-data


# License

This code is licensed under the [EUPL](LICENSE.txt).


# How to install

1. checkout from Git: https://gitlab.com/po-kraj-sk/nczi-covid19-v2-harvester.git
2. create virtual environment: `virtualenv --python=python3.7 .venv`
3. activate: `source .venv/bin/activate`
4. install dependencies: `pip install -r requirements.txt`
5. create configuration file: `cp nczi_covid19_harvester.conf.template nczi_covid19_harvester.conf`

Then also:

6. inspect configuration in `nczi_covid19_harvester.conf`, adjust if necessary
7. check that database exists along with schema 'nczi'
8. check user running harvester is able to create tables in the database schema
9. create directory where local copy of data will be klept (`repo_local`
   item in `nczi_covid19_harvester.conf`)


# How to run

`python -m nczi_covid19_harvester`


## Updates of dataset schema

As of now, code does not handle changes in dataset structure (i.e. DB
schema updates).

If the structure changes, DB (or affected tables) needs to be dropped and
all data downloaded again.

Or DB schema (table names and structure) needs to be updated by hand.
