# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.

import logging

from pandas import DataFrame

from .common import DB_SCHEMA, HarvesterBase


class PositiveTestsAGDHarvester(HarvesterBase):

    CONFIG_SECTION = 'positive_tests_age_group_district'

    CSV_FN = 'OpenData_Slovakia_Covid_PositiveTests_AgeGroup_District.csv'
    CSV_SUBDIR = 'PCR_Tests'
    CSV_ENCODING = [ 'UTF-8', 'cp1250' ]
    DATE_COLUMNS = ['Date']
    DATE_FORMAT = '%Y-%m-%d'

    TABLE_NAME = 'positive_tests_age_group_district'
