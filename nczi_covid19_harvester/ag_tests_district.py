# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.

import configparser
import logging

from pandas import DataFrame

from .common import DB_SCHEMA, HarvesterBase


class AgTestsDistrict(HarvesterBase):

    CONFIG_SECTION = 'ag_tests_district'

    CSV_FN = 'OpenData_Slovakia_Covid_AgTests_District.csv'
    CSV_SUBDIR = 'AG_Tests'
    CSV_ENCODING = [ 'UTF-8', 'cp1250' ]
    DATE_COLUMNS = ['Date']
    DATE_FORMAT = '%Y-%m-%d'

    TABLE_NAME = 'ag_tests_district'


    def __init__(self, cfg_parser, common_resources):
        super().__init__(cfg_parser, common_resources)

        self.districts = cfg_districts = None
        try:
            cfg_districts = cfg_parser.get(self.CONFIG_SECTION, 'filter_district', fallback=None)
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logging.error(e)
            sys.exit(-1)

        if cfg_districts:
            self.districts = cfg_districts.split(',')


    def clean_data(self, df):
        df = super().clean_data(df)

        if self.districts:
            df = df[df['district'].isin(self.districts)]
            logging.debug('shortened data: %s' % df)
        return df
