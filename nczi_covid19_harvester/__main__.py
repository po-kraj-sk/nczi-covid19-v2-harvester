#!/usr/bin/python3
#
# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
#
# This harvests and keeps up-to-date statistical data from NCZI
# in local PostgreeSQL database.
#
# Main points:
# a) on first run creates tables in empty DB and gets (clone) all data from NCZI
# b) on subsequent runs deletes what is in DB and gets again (pull) all from NCZI
# c) one CSV = one dataset = one DB table
# d) since Pandas' capability to load CSVs into existing tals is limited,
#    2nd and subsequent runs use temporary tables, then copying data to existing
#    tables; may fail when structure changes, on the other hand allow stuff to be
#    created on-top of those tables independently of the harvester (say views, etc.
#    which would either prevent tables to be dropped or be lost of tables are
#    to be dropped with CASCADE)

import configparser
import logging
import sys
from argparse import ArgumentParser

import git

from sqlalchemy.engine.reflection import Inspector
from sqlalchemy.exc import IntegrityError
from sqlalchemy.schema import AddConstraint, ForeignKeyConstraint, PrimaryKeyConstraint

from . import *
from .common import DB_SCHEMA
from .common import CommonHarvesterResources


class NcziCovid19Harvester:

    CONFIG_FILE_NAME = 'nczi_covid19_harvester.conf'

    HARVESTERS = {
        'ag_test_district': AgTestsDistrict,
        'hospital': HospitalHarvester,
        'positive_test_agd': PositiveTestsAGDHarvester
    }


    def __init__(self):
        # basic logging setup (step 1)
        logging.basicConfig(
            format='%(asctime)s %(levelname)s %(message)s',
            level=logging.INFO)

        # load config file
        cfg_parser = self._load_config()

        # basic logging setup (step 2)
        self._logging_setup(cfg_parser)

        # common resources
        self.common_resources = CommonHarvesterResources(cfg_parser)

        # command-line options
        harvester_names = self._parse_cmdln_options()
        if len(harvester_names) <= 0:
            harvester_names = self.HARVESTERS.keys()

        # initialize desired harvesters
        self.harvesters = []
        for harvester_name in harvester_names:
            try:
                self.harvesters.append(self.HARVESTERS[harvester_name](cfg_parser, self.common_resources))
            except RuntimeError as e:
                logging.error(e)
                logging.error(traceback.format_exc())


    def _parse_cmdln_options(self):
        parser = ArgumentParser()
        parser.add_argument(
            '-f', '--force',
            action="store_true", dest="force", default=False,
            help="perform update even if source is not changed")
        parser.add_argument(
            'harvesters', metavar='harvester', nargs='*',
            help='a harvester to run')
        parser.add_argument(
            '-v', '--verbose',
            action="store_true", dest="verbose", default=False,
            help="be more verbose")
        args = parser.parse_args()

        if args.force:
            self.common_resources.cfg_force = args.force
        if args.verbose:
            logging.getLogger().setLevel('DEBUG')
        # TODO: add `list` to list available harvesters

        return args.harvesters


    def _load_config(self):
        cfg_parser = configparser.ConfigParser()
        cfg_parser.read(self.CONFIG_FILE_NAME)
        return cfg_parser


    def _logging_setup(self, cfg_parser):
        try:
            # mandatory:
            self.cfg_repo_url = cfg_parser.get(CommonHarvesterResources.CONFIG_SECTION_MAIN, 'repo_url')
            # optional:
            cfg_log_level = cfg_parser.get(CommonHarvesterResources.CONFIG_SECTION_MAIN, 'log_level', fallback='INFO')
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logging.error('%s (file %s)' % (e, self.CONFIG_FILE_NAME))
            sys.exit(-1)

        logging.getLogger().setLevel(cfg_log_level)


    def clone_repo(self):
        logging.info('cloning %s to %s' % (self.cfg_repo_url, self.common_resources.cfg_repo_local))
        git_repo = git.Repo.clone_from(self.cfg_repo_url, self.common_resources.cfg_repo_local)
        return git_repo


    def update_repo(self):
        git_repo = None
        try:
            git_repo = git.Repo(self.common_resources.cfg_repo_local)
        except git.exc.InvalidGitRepositoryError:
            git_repo = self.clone_repo()

        origin = git_repo.remotes['origin']
        logging.info('pulling updates from %s to %s' % (origin.url, self.common_resources.cfg_repo_local))
        origin.pull()
        # TODO: detect whether something new was pulled and return indication


    def main(self):
        # clone/pull repo with data
        self.update_repo()
        # TODO: detect whether something new was pulled and return from here if nothing new

        # extract data
        for harvester in self.harvesters:
            harvester.run()


if __name__ == '__main__':
    nczi_covid19_harvester = NcziCovid19Harvester()
    nczi_covid19_harvester.main()
