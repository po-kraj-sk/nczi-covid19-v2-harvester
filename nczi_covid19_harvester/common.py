# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.

import configparser
import locale
import logging
import os
import sys
import tempfile
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

from git import Repo

import pandas

import sqlalchemy
from sqlalchemy import create_engine, inspect
from sqlalchemy import MetaData
from sqlalchemy import Column
from sqlalchemy import DateTime, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound


DB_SCHEMA = 'nczi'


Base = declarative_base(metadata=MetaData(schema=DB_SCHEMA))


class DatasetMetadata(Base):
    __tablename__ = 'metadata_dataset'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    lmt = Column(DateTime)

    def __repr__(self):
        return "<Dataset(id='%s', name='%s', last_update='%s', ...)>" % (
            self.id, self.name, self.last_update)


class CommonHarvesterResources():

    CONFIG_SECTION_MAIN = 'nczi_covid19_harvester'
    CONFIG_SECTION_SQLALCHEMY = 'sqlalchemy'


    def __init__(self, cfg_parser):
        # parse configuration
        try:
            # mandatory:
            cfg_db_string = cfg_parser.get(self.CONFIG_SECTION_MAIN, 'db')
            self.cfg_repo_local = cfg_parser.get(self.CONFIG_SECTION_MAIN, 'repo_local')
            # optional:
            self.cfg_force = cfg_parser.get(self.CONFIG_SECTION_MAIN, 'force', fallback=False)
            cfg_sqlalchemy_echo = cfg_parser.getboolean(self.CONFIG_SECTION_SQLALCHEMY, 'echo', fallback=False)
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logging.error(e)
            sys.exit(-1)

        self.engine = create_engine(cfg_db_string, echo=cfg_sqlalchemy_echo)
        self.Session = sessionmaker(self.engine)
        self.session = self.Session()

        self.inspect = inspect(self.engine)

        Base.metadata.create_all(self.engine, checkfirst=True)
        self.metadata = Base.metadata
        self.metadata.bind = self.engine
        self.metadata.reflect(schema=DB_SCHEMA)


class HarvesterBase():

    CONFIG_SECTION = None

    CSV_FN = None
    CSV_SUBDIR = None
    CSV_SEPARATOR = ';'
    CSV_ENCODING = [ 'UTF-8' ]
    DATE_COLUMNS = False
    DATE_FORMAT = None

    TABLE_NAME = None

    SKIP_FIRST_COLUMN = False


    def __init__(self, cfg_parser, common_resources):
        self.common_resources = common_resources
        self.index_columns = None

        # parse configuration
        self.cfg_history = -1
        try:
            # optional:
            self.cfg_history = cfg_parser.getint(self.CONFIG_SECTION, 'history', fallback=-1)
            cfg_index_columns = cfg_parser.get(self.CONFIG_SECTION, 'index_columns', fallback=None)
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            logging.error(e)
            sys.exit(-1)

        if cfg_index_columns:
            self.index_columns = cfg_index_columns.split(',')


    def get_dataset_metadata(self):
        dm = None
        try:
            dm = self.common_resources.session.query(DatasetMetadata).filter(DatasetMetadata.name == self.CSV_FN).one()
        except NoResultFound:
            dm = DatasetMetadata(name=self.CSV_FN)
            dm.lmt = datetime.fromtimestamp(0)
            self.common_resources.session.add(dm)

        return dm


    @staticmethod
    def get_lmt(fn):
        mtime = datetime.fromtimestamp(os.path.getmtime(fn))
        return mtime


    def read_csv(self, fn):
        logging.info('reading %s' % fn)

        parser = None
        if self.DATE_FORMAT:
            parser = lambda date: datetime.strptime(date, self.DATE_FORMAT)

        encoding_exception = None
        for csv_encoding in self.CSV_ENCODING:
            try:
                df = pandas.read_csv(fn,
                    sep=self.CSV_SEPARATOR,
                    encoding=csv_encoding,
                    parse_dates=self.DATE_COLUMNS,
                    date_parser=parser)
                encoding_exception = None
                break
            except UnicodeDecodeError as e:
                encoding_exception = e
                logging.warning('CSV decoding failed when %s used' % csv_encoding)
        if encoding_exception:
            raise encoding_exception
        logging.debug('data: %s' % df)

        return df


    def clean_data(self, df):
        # some datasets use first column to store ID, we do not need those IDs
        if self.SKIP_FIRST_COLUMN:
            df = df.drop(df.columns[0], axis=1)

        # rename columns to lower-case for convenience
        renames = {}
        for column_name in df.columns:
            new_name = column_name.lower()
            if new_name != column_name:
                renames[column_name] = new_name
        if len(renames) > 0:
            df.rename(columns=renames, inplace=True)

        # trim old data
        if self.cfg_history > 0:
            if len(self.DATE_COLUMNS) >= 1:
                filter_data_column = self.DATE_COLUMNS[0].lower()
                if len(self.DATE_COLUMNS) > 1:
                    logging.warning('multiple date columns present, using %s for history filtering' % filter_data_column)

                x_days_ago = datetime.now() - relativedelta(days=self.cfg_history)
                df = df[df[filter_data_column] >= x_days_ago]
                logging.debug('shortened data: %s' % df)

            else:
                logging.debug('no date columns present, history setting igored')

        logging.debug('cleaned data: %s' % df)
        return df


    def set_indexes(self, df):
        if self.index_columns:
            df.set_index(self.index_columns, append=False, inplace=True, verify_integrity=True)

        logging.debug('data with indexes: %s' % df)
        return df


    def store_table(self, df, name):
        # To allow views, triggers, etc. to be defined for target table, we'd like
        # to avoid dropping that table. But drop is required by pandas in order
        # to properly propagate new data and delete old data. Thus we:

        # 0) does that table already exist?
        table_exists = self.common_resources.inspect.has_table(name, schema=DB_SCHEMA)
        delete_count = 0

        # 1) write panda frame to temporary table
        tmp_name = name + '_tmp'
        if not table_exists:
            tmp_name = name
        df.to_sql(
            tmp_name,
            self.common_resources.engine,
            schema=DB_SCHEMA
            )
        self.common_resources.session.commit()
        logging.debug('%d items written to table %s' % (df.shape[0], tmp_name))

        if table_exists:
            # 2) delete data from target table
            table = self.common_resources.metadata.tables[DB_SCHEMA + '.' + name]
            query = sqlalchemy.delete(table)
            r = self.common_resources.session.execute(query)
            delete_count = r.rowcount
            logging.debug('%s items deleted from table %s' % (r.rowcount, name))

            # 3) copy data from temporary to target table
            tmp_table = sqlalchemy.Table(
                tmp_name,
                self.common_resources.metadata,
                autoload=True,
                autoload_with=self.common_resources.engine)
            names = table.columns
            query = sqlalchemy.insert(table).from_select(names, tmp_table.select())
            r = self.common_resources.session.execute(query)
            logging.debug('%s items copied from table %s to table %s' % (r.rowcount, tmp_name, name))
            self.common_resources.session.commit()

            # 4) delete temporary table
            tmp_table.drop()

        logging.info('%d new items written to table %s and deleted %d old items' % (df.shape[0], name, delete_count))


    def store_dataset(self, df):
        self.store_table(df, self.TABLE_NAME)


    def run(self):
        # check for updates
        fn = os.path.join(
            self.common_resources.cfg_repo_local,
            self.CSV_SUBDIR if self.CSV_SUBDIR else '',
            self.CSV_FN)
        dm = self.get_dataset_metadata()
        lmt = self.get_lmt(fn)
        if dm.lmt == HarvesterBase.get_lmt(fn) and not self.common_resources.cfg_force:
            logging.info('skipping %s - not changed since last run' % self.CSV_FN)
            return

        # do the ETL work
        df = self.read_csv(fn)
        df = self.clean_data(df)
        df = self.set_indexes(df)
        self.store_dataset(df)

        # all done, mark the update
        dm.lmt = lmt
        self.common_resources.session.commit()
