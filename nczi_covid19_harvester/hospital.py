# Copyright 2020 by Peter Hanecak <hanecak+wb@hany.sk>
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved
# by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.

import configparser
import logging
from datetime import datetime

from pandas import DataFrame

from .common import DB_SCHEMA, HarvesterBase


class HospitalHarvester(HarvesterBase):

    CONFIG_SECTION = 'hospital'

    CSV_FN = 'OpenData_Slovakia_Covid_Hospital.csv'
    CSV_SUBDIR = 'Hospitals'
    DATE_COLUMNS = ['Datum']
    DATE_FORMAT = '%Y-%m-%d'

    TABLE_NAME = 'covid_hospital'
    SKIP_FIRST_COLUMN = True
