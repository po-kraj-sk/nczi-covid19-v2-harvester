CREATE INDEX IF NOT EXISTS ix_nczi_positive_tests_age_group_district_agegroup ON nczi.positive_tests_age_group_district (agegroup);
CREATE INDEX IF NOT EXISTS ix_nczi_positive_tests_age_group_district_date ON nczi.positive_tests_age_group_district (date);
CREATE INDEX IF NOT EXISTS ix_nczi_positive_tests_age_group_district_disctrict ON nczi.positive_tests_age_group_district (disctrict);
CREATE INDEX IF NOT EXISTS ix_nczi_positive_tests_age_group_district_gender ON nczi.positive_tests_age_group_district (gender);

CREATE OR REPLACE VIEW nczi.sums_positive_tests_age_group_district AS
SELECT
	date,
	disctrict,
	count(1) AS prirastiok_spolu,
	count(1) FILTER (WHERE gender = 'Muž') AS prirastok_muzi,
	count(1) FILTER (WHERE gender = 'Žena') AS prirastok_zeny,
	count(1) FILTER (WHERE agegroup <= 30) AS pocet_do_30,
	count(1) FILTER (WHERE agegroup > 30 AND agegroup < 65) AS pocet_30_65,
	count(1) FILTER (WHERE agegroup >= 65) AS pocet_od_65
FROM nczi.positive_tests_age_group_district
GROUP BY date, disctrict
ORDER BY date, disctrict;
