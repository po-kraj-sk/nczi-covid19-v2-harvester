#!/bin/bash

LOG=nczi_covid19_harvester.log

cd `dirname $0`
. .venv/bin/activate

python -m nczi_covid19_harvester &> $LOG
